# ---------------------
# Numbering a Crossword
# _____________________

grid = [
    "WWWWWWWWBWWWWWW",
    "WBWBWBWBBBWBWBW",
    "WWWWWWWWBWWWWWW",
    "WBWBWBWBWBWBWBW",
    "WWWWWWWWWWWBBBB",
    "WBWBBBWBWBWBWBW",
    "WWWWWBWWWWWWWWW",
    "WBWBWBWBWBWBWBW",
    "WWWWWWWWWBWWWWW",
    "WBWBWBWBWBBBWBW",
    "BBBBBWWWWWWWWWW",
    "WBWBWBWBWBWBWBW",
    "WWWWWWBWWWWWWWW",
    "WBWBWBBBWBWBWBW",
    "WWWWWWBWWWWWWWW"
]

WHITE = 'W'
BLACK = 'B'
SIZE = 15

def check_black(x: int, y: int) -> bool:
    return grid[x][y] == BLACK

def check_across(x: int, y: int) -> tuple[int, int]:
    end_x, end_y = 0, 0
    while check_black(end_x - 1, end_y) and not check_black(end_x + 1, end_y):
        end_x, end_y = end_x + 1, end_y
    return end_x, end_y

def check_down(x: int, y: int) -> tuple[int, int]:
    end_x, end_y = 0, 0
    while check_black(x, y - 1) and not check_black(end_x, end_y + 1):
        end_x, end_y = end_x, end_y + 1
    return end_x, end_y

def traverse_grid(grid: list[str]):      
    x, y = 0, 0
    num = 1
    numbered_pos = []
    while x, y < SIZE, SIZE:
        grid[x][y] 
