# 

def word_start(grid, row, col) -> bool:
    if grid[row][col] == 'B':
        return False

    if (col == 0 or grid[row][col - 1] == 'B') and (col + 1 < len(grid[row]) and grid[row][col + 1] != 'B'):
        return True

    if (row == 0 or grid[row - 1][col] == 'B') and (row + 1 < len(grid) and grid[row + 1][col] != 'B'):
        return True
    return False

def numbering(grid) -> bool:
    number = 1
    word_number = []

    for r in range(len(grid)):
        for c in range(len(grid[r])):
            if word_start(grid, r, c):
                word_number.append((r, c, number))
                number += 1
    return word_number

grid =  [
    'WWWWWWWWBWWWWWW',
    'WBWBWBWBBBWBWBW',
    'WWWWWWWWBWWWWWW',
    'WBWBWBWBWBWBWBW',
    'WWWWWWWWWWWBBBB',
    'WBWBBBWBWBWBWBW',
    'WWWWWBWWWWWWWWW',
    'WBWBWBWBWBWBWBW',
    'WWWWWWWWWBWWWWW',
    'WBWBWBWBWBBBWBW',
    'BBBBBWWWWWWWWWW',
    'WBWBWBWBWBWBWBW',
    'WWWWWWBWWWWWWWW',
    'WBWBWBBBWBWBWBW',
    'WWWWWWBWWWWWWWW'
]
print(numbering(grid))
